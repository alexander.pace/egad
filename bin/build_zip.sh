#!/bin/bash

# Fail if there are any errors (-e) and undefined variables (-u)
set -eu

# Ensure an EGAD version was specified.
if [[ -z "$EGAD_VERSION" ]]
then
    >&2 echo "Environment variable EGAD_VERSION must be set to the version to be published"
    exit 1
fi

# Ensure a Python version was specified
if [[ -z "$PYTHON_VERSION" ]]
then
    >&2 echo "Environment variable PYTHON_VERSION must be set to the Python version to use"
    exit 1
fi

# Record original directory
BASE_DIR=$PWD

# Ensure `build/` directory exists and enter it
mkdir -p build
cd build

VERSION_SUFFIX="${EGAD_VERSION}-${PYTHON_VERSION}"

PACKAGE_DIR="package-${VERSION_SUFFIX}/"
ZIP_FILE="package-${VERSION_SUFFIX}.zip"
VENV_DIR="venv-${VERSION_SUFFIX}/"

# Delete any existing package directory, and create a new one.
rm -rf "$PACKAGE_DIR"
mkdir -p "$PACKAGE_DIR"

# Clear existing virtualenv, recreate, and activate
rm -rf "$VENV_DIR"
virtualenv -p "$PYTHON_VERSION" "$VENV_DIR"
source "${VENV_DIR}/bin/activate"

# Install dependencies
"${VENV_DIR}/bin/pip" install -r "${BASE_DIR}/requirements.txt" --target "$PACKAGE_DIR"

# Copy Lambda function to package
cp -r "${BASE_DIR}/lambda_function/" "${PACKAGE_DIR}/lambda_function/"

# Copy virtualenv's site-packages to package
cp -r "${VENV_DIR}/lib/${PYTHON_VERSION}/site-packages/"* "$PACKAGE_DIR"

# Delete any existing Zip file and create a new one
rm -f "$ZIP_FILE"
cd "$PACKAGE_DIR" && zip -r "../$ZIP_FILE" "."
