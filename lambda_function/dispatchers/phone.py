from hashlib import sha1
import os
import time

from aws_xray_sdk.core import xray_recorder

import twilio.rest

from .base import Dispatcher
from ..secrets import get_secrets

from ..logging import getLogger
logger = getLogger(__name__)

_tier_to_twilio_client = {}


class PhoneDispatcher(Dispatcher):
    @xray_recorder.capture("## init_phone")
    def __init__(self, tier):
        if tier not in _tier_to_twilio_client:
            time_start = time.perf_counter()

            region_name = os.environ.get("SECRETS_REGION", "us-west-2")
            secret_name = f"EGAD_Credentials/{tier}"

            secrets = get_secrets(region_name, secret_name)

            username = secrets["TWILIO_ACCOUNT_SID"]
            password = secrets["TWILIO_AUTH_TOKEN"]
            preferred_number = secrets["TWILIO_PREFERRED_NUMBER"]

            _tier_to_twilio_client[tier] = (
                TwilioClientWrapper(username=username, password=password,
                                    preferred_number=preferred_number)
            )

            time_elapsed = time.perf_counter() - time_start
            logger.info("Connected Twilio client in %.1f sec", time_elapsed)

        else:
            logger.info("Using existing Twilio client")

        self.client = _tier_to_twilio_client[tier]

    @xray_recorder.capture("## handle_phone")
    def _handle(self, contents, context) -> tuple[int, str]:
        contacts = contents.contacts
        message = contents.message
        twiml_url = contents.twiml_url

        for contact in contacts:
            phone_method = contact.phone_method
            phone_number = contact.phone_number

            if phone_method in ["C", "B"]:
                self.client.call(phone_number, twiml_url)

            if phone_method in ["T", "B"]:
                self.client.text(phone_number, message)

        return 200, "Success"


class TwilioClientWrapper:
    def __init__(self, username, password, preferred_number):
        self._client = twilio.rest.Client(username=username, password=password)
        self._incoming_phone_numbers = (
            self._client.incoming_phone_numbers.list()
        )

        if len(self._incoming_phone_numbers) == 0:
            raise RuntimeError(
                f"No phone numbers associated with "
                f"Twilio account {self._client.username}"
            )

        # TODO: Set up country-specific numbers if we get an international
        #       number.
        if preferred_number in [num.phone_number for
                                num in self._incoming_phone_numbers]:
            self._default_from_number = preferred_number
            logger.info("Twilio defaulting to preferred number %s",
                        preferred_number)
        else:
            self._default_from_number = (
                self._incoming_phone_numbers[0].phone_number
            )
            logger.warning(
                "%s not associated with access token, sending with %s",
                preferred_number, self._default_from_number)

    def call(self, phone_number, twiml_url):
        message_id = sha1(
            f"{phone_number}:{twiml_url}".encode("utf-8")
        ).hexdigest()
        log_common_str = f"{phone_number} with message_id {message_id}"

        logger.info("Calling %s", log_common_str)

        try:
            self._client.calls.create(
                to=phone_number, from_=self._default_from_number,
                url=twiml_url, method='GET',
            )

        except Exception:
            logger.exception("Failed to call %s", log_common_str)

    def text(self, phone_number, message):
        message_id = sha1(
            f"{phone_number}:{message}".encode("utf-8")
        ).hexdigest()
        log_common_str = f"{phone_number} with message_id {message_id}"

        logger.info("Texting %s", log_common_str)

        try:
            self._client.messages.create(
                to=phone_number, from_=self._default_from_number,
                body=message,
            )

        except Exception:
            logger.exception("Failed to text %s", log_common_str)
