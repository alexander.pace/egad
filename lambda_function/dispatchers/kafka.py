from hashlib import sha1
from itertools import islice
import os
import time

from aws_xray_sdk.core import xray_recorder

import igwn_alert

from .base import Dispatcher
from ..secrets import get_secrets
from ..status import is_provisioned_concurrency

from ..logging import getLogger
logger = getLogger(__name__)

TOPIC_BATCH_SIZE = 8

_igwn_alert_clients = {}


class KafkaDispatcher(Dispatcher):
    @xray_recorder.capture("## init_kafka")
    def __init__(self, tier):
        if tier not in _igwn_alert_clients:
            time_start = time.perf_counter()

            region_name = os.environ.get("SECRETS_REGION", "us-west-2")
            secret_name = f"EGAD_Credentials/{tier}"

            secrets = get_secrets(region_name, secret_name)

            username = secrets["SCIMMA_USERNAME"]
            password = secrets["SCIMMA_PASSWORD"]
            server = secrets["IGWN_ALERT_KAFKA_SERVER"]
            group = secrets["IGWN_ALERT_GROUP"]

            logger.info("Connecting IGWN Alert client to %s", server)
            _igwn_alert_clients[tier] = igwn_alert.client(
                username=username, password=password,
                server=server, group=group,
            )

            if is_provisioned_concurrency:
                self.connect(tier)

            time_elapsed = time.perf_counter() - time_start
            logger.info("Connected IGWN Alert client in %.1f sec",
                        time_elapsed)

        else:
            logger.info("Using existing IGWN Alert client")

        self.client = _igwn_alert_clients[tier]

    @xray_recorder.capture("## handle_kafka")
    def _handle(self, contents, context) -> tuple[int, str]:
        topics = contents.topics
        message = contents.message

        message_id = sha1(
            (",".join(topics) + message).encode('utf-8')
        ).hexdigest()

        try:
            logger.info("Sending %s to topics %s", message_id, topics)
            for topic in topics:
                if topic in self.client.sessions:
                    logger.info("Publishing to %s using established session",
                                topic)
                    self.client.publish_to_topic(topic, msg=message)
                else:
                    logger.info("Publishing to %s using one-off session",
                                topic)
                    self.client.publish(topic, msg=message)
            logger.info("Sent %s to topics %s", message_id, topics)
        except Exception:
            logger.exception("Failed to send %s to topics %s",
                             message_id, topics)
            return 422, f"Failed to send message {message_id}"

        return 200, "Success"

    @xray_recorder.capture("### connect_kafka")
    def connect(self, tier):
        client = _igwn_alert_clients[tier]
        logger.info("Getting IGWN Alert topic list")
        all_topics = client.get_topics()

        # Only including test topics
        all_topics = [topic for topic in all_topics if "test" in topic]

        logger.info("Connecting to IGWN Alert topics: %s",
                    ", ".join(all_topics))
        time_start = time.perf_counter()
        client.connect(all_topics)
        time_elapsed = time.perf_counter() - time_start
        logger.info("Connected to IGWN Alert topics in %.1f sec", time_elapsed)


# Taken from itertools docs
def batched(iterable, n):
    "Batch data into tuples of length n. The last batch may be shorter."
    # batched('ABCDEFG', 3) --> ABC DEF G
    if n < 1:
        raise ValueError('n must be at least one')
    it = iter(iterable)
    while (batch := tuple(islice(it, n))):
        yield batch
