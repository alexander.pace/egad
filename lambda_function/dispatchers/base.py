import abc

from .. import responses

from ..logging import getLogger
logger = getLogger(__name__)


class Dispatcher(metaclass=abc.ABCMeta):
    def handle(self, contents, context) -> str:
        cls_name = self.__class__.__name__
        logger.info("Handling alert with: %s", cls_name)

        try:
            status, message = self._handle(contents, context)
        except Exception:
            logger.exception("Uncaught exception in handler: %s", cls_name)

            status = 500
            message = (
                f"Unhandled error, see log stream '{context.log_stream_name}'"
            )

        return responses.custom(
            statusCode=status,
            body=message,
        )

    @abc.abstractmethod
    def _handle(self, contents, context) -> tuple[int, str]:
        """
        Handle event and return HTTP status code and message.
        """
        ...
